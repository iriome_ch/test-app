<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    @if(isset($id))
    <h1>Welcome to Laravel {{$id}}</h1>
    @else
    <h1>Welcome to Laravel</h1>
    @endif
    <p>
        This view is loaded from module: {!! config('hello.name') !!}
    </p>

    @if($_SERVER['REQUEST_METHOD'] == 'POST')
    <p>
        This view is a POST request
    </p>
    @else
    <form action="/ho" method="post">
        @csrf
        <input type="submit" value="">
    </form>
    @endif
</body>

</html>