<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Crea una ruta que tenga un parámetro que sea opcional y comprueba que funciona.
Route::get('/home/{id?}', function ($id = null) {
    return view('home', ['id' => $id]);
});
// Crea una ruta que tenga un parámetro que sea opcional y tenga un valor por defecto en caso de que no se especifique.
Route::get('/hi/{id?}', function ($id = 12345) {
    return view('home', ['id' => $id]);
});

// Crea una ruta que atienda por POST y compruébala con Postman. Si obtienes un error de tipo VerifyCsrfToken comenta la línea correspondiente en el fichero kernel.php (carpeta Http). Esto es un filtro para evitar ataques XSS (Cross-Site Scripting). Vuelve a descomentarla cuando termines con las prácticas.
Route::post('/ho', function () {
    return view('home');
});
// Crea una ruta que atienda por GET y por POST (en un único método) y compruébalas. Vuelve a habilitar el filtro VerifyCsrfToken en el fichero kernel.php (carpeta Http).
Route::match(['get', 'post'], '/hou', function () {
    return view('home');
});

// Crea una ruta que compruebe que un parámetro está formado sólo por números.

Route::get('/hu/{id}', function ($id) {
    return view('home', ['id' => $id]);
})->where('id', '[0-9]+');

// Crea una ruta con dos parámetros que compruebe que el primero está formado sólo por letras y el segundo sólo por números.
Route::get('/ha/{name}/{id}', function ($name, $id) {
    return view('home', ['name' => $name, 'id' => $id]);
})->where(['name' => '[a-zA-Z]+', 'id' => '[0-9]+']);

// Utiliza el helper env para que cuando se acceda a la ruta /host nos devuleva la dirección IP donde se encuentra la base de datos de nuestro proyecto.
Route::get('/host', function () {
    return env('DB_HOST');
});
// Utiliza el helper config para que cuando se acceda a la ruta /timezone se muestre la zona horaria.
Route::get('/timezone', function () {
    return config('app.timezone');
});

// Define una vista llamada home.blade.php que muestre "Esta es mi primera vista en Laravel" al acceder a la ruta /inicio de tu proyecto. Utiliza Route::view.
Route::get('/inicio', function () {
    return view('home_view', ['title' => 'Esta es mi primera vista en Laravel', 'content' => 'Esta es mi primera vista en Laravel']);
});

// Crea otra vista que se llame fecha.blade.php y crea una ruta en /fecha. La ruta le pasará a la vista un array asociativo para que se muestre la fecha sacando por pantalla las variables de dicho array. El día estará en una variable, el mes en otra y el año en otra (puedes usar la función date() de PHP). Utiliza el helper view.
Route::get('/fecha', function () {
    $fecha = [
        'dia' => date('d'),
        'mes' => date('m'),
        'anio' => date('Y')
    ];
    return view('fecha', ['fecha' => $fecha]);
});

// Haz lo mismo pero con la función PHP compact.
Route::get('/fecha2', function () {
    $dia = date('d');
    $mes = date('m');
    $anio = date('Y');
    $fecha = compact('dia', 'mes', 'anio');
    return view('fecha', ['fecha' => $fecha]);
});
// Haz lo mismo pero con el helper with.
Route::get('/fecha3', function () {
    $dia = date('d');
    $mes = date('m');
    $anio = date('Y');
    $fecha = with($dia ,function ($dia) use ($mes, $anio) {
        return compact('dia', 'mes', 'anio');
    });
    return view('fecha', ['fecha' => $fecha]);
});

// Cargar imágenes desde blade. Crea una carpeta images en el directorio public y dentro sube una imagen 404.jpg personalizada. Crea una vista de prueba que acceda a la imagen utilizando el helper asset(images/404.jpg) y comprueba que funciona (<img src="{{asset('images/404.jpg')}}" alt="Error 404">).
Route::get('/imagen', function () {
    return view('404', ['imagen' => asset('images/error-404.jpg')]);
});

use Illuminate\Support\Facades\DB;
Route::get('/consultas', function () {
    $users = DB::table('users')->delete(3);
    if ($users) {
        return $users;
    }
});